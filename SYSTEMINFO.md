# Qualys

Vendor: Qualys
Homepage: https://www.qualys.com/

Product: Qualys
Product Page: https://www.qualys.com/

## Introduction
We classify Qualys into the Security domain as Qualys provides access to a wide range of security-related functionalities and features.

"Qualys is a proactive solution, which informs you of known vulnerabilities in your infrastructure." 

## Why Integrate
The Qualys adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Qualys platform to provide comprehensive security and compliance solutions.

With this adapter you have the ability to perform operations with Qualys such as:

- Events
- Incidents
- Asset tagging
- Define, manage, and enforce security policies

## Additional Product Documentation
The [API documents for Qualys](https://docs.qualys.com/en/vm/api/index.htm)