/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-qualys',
      type: 'Qualys',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Qualys = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Qualys Adapter Test', () => {
  describe('Qualys Class Tests', () => {
    const a = new Qualys(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('qualys'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.7.3', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.14.2', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('qualys'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Qualys', pronghornDotJson.export);
          assert.equal('Qualys', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-qualys', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('qualys'));
          assert.equal('Qualys', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-qualys', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-qualys', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#testVMAPIAccess - errors', () => {
      it('should have a testVMAPIAccess function', (done) => {
        try {
          assert.equal(true, typeof a.testVMAPIAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#testKnowledgeBaseAccess - errors', () => {
      it('should have a testKnowledgeBaseAccess function', (done) => {
        try {
          assert.equal(true, typeof a.testKnowledgeBaseAccess === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.testKnowledgeBaseAccess(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-testKnowledgeBaseAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing details', (done) => {
        try {
          a.testKnowledgeBaseAccess('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'details is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-testKnowledgeBaseAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.testKnowledgeBaseAccess('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-testKnowledgeBaseAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing echoRequest', (done) => {
        try {
          a.testKnowledgeBaseAccess('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'echoRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-testKnowledgeBaseAccess', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getyourportalversion - errors', () => {
      it('should have a getyourportalversion function', (done) => {
        try {
          assert.equal(true, typeof a.getyourportalversion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#step1DownloadaListofallHoststobePurgedForyourrecords - errors', () => {
      it('should have a step1DownloadaListofallHoststobePurgedForyourrecords function', (done) => {
        try {
          assert.equal(true, typeof a.step1DownloadaListofallHoststobePurgedForyourrecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.step1DownloadaListofallHoststobePurgedForyourrecords(null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1DownloadaListofallHoststobePurgedForyourrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing echoRequest', (done) => {
        try {
          a.step1DownloadaListofallHoststobePurgedForyourrecords('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'echoRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1DownloadaListofallHoststobePurgedForyourrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing noVmScanSince', (done) => {
        try {
          a.step1DownloadaListofallHoststobePurgedForyourrecords('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'noVmScanSince is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1DownloadaListofallHoststobePurgedForyourrecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#step1LaunchReportthatSupportRequested - errors', () => {
      it('should have a step1LaunchReportthatSupportRequested function', (done) => {
        try {
          assert.equal(true, typeof a.step1LaunchReportthatSupportRequested === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.step1LaunchReportthatSupportRequested(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1LaunchReportthatSupportRequested', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateId', (done) => {
        try {
          a.step1LaunchReportthatSupportRequested('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'templateId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1LaunchReportthatSupportRequested', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing reportTitle', (done) => {
        try {
          a.step1LaunchReportthatSupportRequested('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'reportTitle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1LaunchReportthatSupportRequested', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outputFormat', (done) => {
        try {
          a.step1LaunchReportthatSupportRequested('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'outputFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-step1LaunchReportthatSupportRequested', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addUsersNotbulk - errors', () => {
      it('should have a addUsersNotbulk function', (done) => {
        try {
          assert.equal(true, typeof a.addUsersNotbulk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.addUsersNotbulk(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sendEmail', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'sendEmail is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userRole', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'userRole is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing firstName', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'firstName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lastName', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'lastName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing title', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'title is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing phone', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'phone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing email', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'email is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address1', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'address1 is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing city', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'city is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing country', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'country is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.addUsersNotbulk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-addUsersNotbulk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bulkActivateAgentsforVMModule - errors', () => {
      it('should have a bulkActivateAgentsforVMModule function', (done) => {
        try {
          assert.equal(true, typeof a.bulkActivateAgentsforVMModule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing module', (done) => {
        try {
          a.bulkActivateAgentsforVMModule(null, (data, error) => {
            try {
              const displayE = 'module is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-bulkActivateAgentsforVMModule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#exportNewActiveReOpenVulnerabilityDatausingHLD - errors', () => {
      it('should have a exportNewActiveReOpenVulnerabilityDatausingHLD function', (done) => {
        try {
          assert.equal(true, typeof a.exportNewActiveReOpenVulnerabilityDatausingHLD === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing status', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'status is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing detectionUpdatedSince', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'detectionUpdatedSince is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing truncationLimit', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'truncationLimit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outputFormat', (done) => {
        try {
          a.exportNewActiveReOpenVulnerabilityDatausingHLD('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'outputFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-exportNewActiveReOpenVulnerabilityDatausingHLD', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadallcloudagentactivationkeys - errors', () => {
      it('should have a downloadallcloudagentactivationkeys function', (done) => {
        try {
          assert.equal(true, typeof a.downloadallcloudagentactivationkeys === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadtheQualysuseractivitylog - errors', () => {
      it('should have a downloadtheQualysuseractivitylog function', (done) => {
        try {
          assert.equal(true, typeof a.downloadtheQualysuseractivitylog === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.downloadtheQualysuseractivitylog(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-downloadtheQualysuseractivitylog', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listAlertsbycriteriaFirstbatchexample - errors', () => {
      it('should have a listAlertsbycriteriaFirstbatchexample function', (done) => {
        try {
          assert.equal(true, typeof a.listAlertsbycriteriaFirstbatchexample === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#enableVulnerabilityManagementandPolicyComplianceHostAssets - errors', () => {
      it('should have a enableVulnerabilityManagementandPolicyComplianceHostAssets function', (done) => {
        try {
          assert.equal(true, typeof a.enableVulnerabilityManagementandPolicyComplianceHostAssets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.enableVulnerabilityManagementandPolicyComplianceHostAssets(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-enableVulnerabilityManagementandPolicyComplianceHostAssets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enableVm', (done) => {
        try {
          a.enableVulnerabilityManagementandPolicyComplianceHostAssets('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'enableVm is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-enableVulnerabilityManagementandPolicyComplianceHostAssets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing enablePc', (done) => {
        try {
          a.enableVulnerabilityManagementandPolicyComplianceHostAssets('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'enablePc is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-enableVulnerabilityManagementandPolicyComplianceHostAssets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ips', (done) => {
        try {
          a.enableVulnerabilityManagementandPolicyComplianceHostAssets('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ips is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-enableVulnerabilityManagementandPolicyComplianceHostAssets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPICommandstoaddIPaddressEsWindows - errors', () => {
      it('should have a aPICommandstoaddIPaddressEsWindows function', (done) => {
        try {
          assert.equal(true, typeof a.aPICommandstoaddIPaddressEsWindows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsWindows(null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsWindows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsWindows('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsWindows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addIps', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsWindows('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'addIps is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsWindows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPICommandstoaddIPaddressEsUnixLinux - errors', () => {
      it('should have a aPICommandstoaddIPaddressEsUnixLinux function', (done) => {
        try {
          assert.equal(true, typeof a.aPICommandstoaddIPaddressEsUnixLinux === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsUnixLinux(null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsUnixLinux', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsUnixLinux('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsUnixLinux', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing addIps', (done) => {
        try {
          a.aPICommandstoaddIPaddressEsUnixLinux('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'addIps is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPICommandstoaddIPaddressEsUnixLinux', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchVulnerabilityScan - errors', () => {
      it('should have a launchVulnerabilityScan function', (done) => {
        try {
          assert.equal(true, typeof a.launchVulnerabilityScan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.launchVulnerabilityScan(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iscannerId', (done) => {
        try {
          a.launchVulnerabilityScan('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'iscannerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scanTitle', (done) => {
        try {
          a.launchVulnerabilityScan('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'scanTitle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.launchVulnerabilityScan('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'optionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.launchVulnerabilityScan('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing priority', (done) => {
        try {
          a.launchVulnerabilityScan('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'priority is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchVulnerabilityScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#launchPolicyComplianceScan - errors', () => {
      it('should have a launchPolicyComplianceScan function', (done) => {
        try {
          assert.equal(true, typeof a.launchPolicyComplianceScan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.launchPolicyComplianceScan(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing iscannerId', (done) => {
        try {
          a.launchPolicyComplianceScan('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'iscannerId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scanTitle', (done) => {
        try {
          a.launchPolicyComplianceScan('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'scanTitle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.launchPolicyComplianceScan('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'optionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.launchPolicyComplianceScan('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing priority', (done) => {
        try {
          a.launchPolicyComplianceScan('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'priority is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-launchPolicyComplianceScan', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#optionalListallExistingAssetGroups - errors', () => {
      it('should have a optionalListallExistingAssetGroups function', (done) => {
        try {
          assert.equal(true, typeof a.optionalListallExistingAssetGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.optionalListallExistingAssetGroups(null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-optionalListallExistingAssetGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing outputFormat', (done) => {
        try {
          a.optionalListallExistingAssetGroups('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'outputFormat is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-optionalListallExistingAssetGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing truncationLimit', (done) => {
        try {
          a.optionalListallExistingAssetGroups('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'truncationLimit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-optionalListallExistingAssetGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createanewDynamicTag - errors', () => {
      it('should have a createanewDynamicTag function', (done) => {
        try {
          assert.equal(true, typeof a.createanewDynamicTag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createanewDynamicTag(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-createanewDynamicTag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchHostsbyTagName - errors', () => {
      it('should have a searchHostsbyTagName function', (done) => {
        try {
          assert.equal(true, typeof a.searchHostsbyTagName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.searchHostsbyTagName(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-searchHostsbyTagName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyingatagortagstomultiplehostassetsHostID - errors', () => {
      it('should have a applyingatagortagstomultiplehostassetsHostID function', (done) => {
        try {
          assert.equal(true, typeof a.applyingatagortagstomultiplehostassetsHostID === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.applyingatagortagstomultiplehostassetsHostID(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-applyingatagortagstomultiplehostassetsHostID', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#generateAuthenticateGenerateToken - errors', () => {
      it('should have a generateAuthenticateGenerateToken function', (done) => {
        try {
          assert.equal(true, typeof a.generateAuthenticateGenerateToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.generateAuthenticateGenerateToken(null, null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-generateAuthenticateGenerateToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.generateAuthenticateGenerateToken('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-generateAuthenticateGenerateToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing token', (done) => {
        try {
          a.generateAuthenticateGenerateToken('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'token is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-generateAuthenticateGenerateToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduleCertViewScanbyFQDNs - errors', () => {
      it('should have a scheduleCertViewScanbyFQDNs function', (done) => {
        try {
          assert.equal(true, typeof a.scheduleCertViewScanbyFQDNs === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs(null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scanTitle', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'scanTitle is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing active', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'active is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing optionId', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'optionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scanType', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'scanType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing occurrence', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'occurrence is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing frequencyWeeks', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'frequencyWeeks is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing weekdays', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'weekdays is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startHour', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startHour is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startMinute', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'startMinute is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing observeDst', (done) => {
        try {
          a.scheduleCertViewScanbyFQDNs('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'observeDst is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-scheduleCertViewScanbyFQDNs', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#downloadallScheduledReportInfo - errors', () => {
      it('should have a downloadallScheduledReportInfo function', (done) => {
        try {
          assert.equal(true, typeof a.downloadallScheduledReportInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.downloadallScheduledReportInfo(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-downloadallScheduledReportInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievedetailedhostinstanceinformation - errors', () => {
      it('should have a retrievedetailedhostinstanceinformation function', (done) => {
        try {
          assert.equal(true, typeof a.retrievedetailedhostinstanceinformation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.retrievedetailedhostinstanceinformation(null, null, null, null, (data, error) => {
            try {
              const displayE = 'fqdn is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-retrievedetailedhostinstanceinformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.retrievedetailedhostinstanceinformation('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-retrievedetailedhostinstanceinformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.retrievedetailedhostinstanceinformation('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-retrievedetailedhostinstanceinformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing service', (done) => {
        try {
          a.retrievedetailedhostinstanceinformation('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'service is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-retrievedetailedhostinstanceinformation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentIOCEventswhereIndicatorScoreisGreaterthan7 - errors', () => {
      it('should have a getCurrentIOCEventswhereIndicatorScoreisGreaterthan7 function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentIOCEventswhereIndicatorScoreisGreaterthan7 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.getCurrentIOCEventswhereIndicatorScoreisGreaterthan7(null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCurrentIOCEventswhereIndicatorScoreisGreaterthan7', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getCurrentIOCEventswhereIndicatorScoreisGreaterthan7('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCurrentIOCEventswhereIndicatorScoreisGreaterthan7', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7 - errors', () => {
      it('should have a getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7 function', (done) => {
        try {
          assert.equal(true, typeof a.getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing state', (done) => {
        try {
          a.getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7(null, null, (data, error) => {
            try {
              const displayE = 'state is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7('fakeparam', null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#excludedhostslist - errors', () => {
      it('should have a excludedhostslist function', (done) => {
        try {
          assert.equal(true, typeof a.excludedhostslist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.excludedhostslist(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-excludedhostslist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vMScannerappliances - errors', () => {
      it('should have a vMScannerappliances function', (done) => {
        try {
          assert.equal(true, typeof a.vMScannerappliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.vMScannerappliances(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-vMScannerappliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#excludedvirtualhostslist - errors', () => {
      it('should have a excludedvirtualhostslist function', (done) => {
        try {
          assert.equal(true, typeof a.excludedvirtualhostslist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.excludedvirtualhostslist(null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-excludedvirtualhostslist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#pCComplianceposture - errors', () => {
      it('should have a pCComplianceposture function', (done) => {
        try {
          assert.equal(true, typeof a.pCComplianceposture === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing action', (done) => {
        try {
          a.pCComplianceposture(null, null, null, null, (data, error) => {
            try {
              const displayE = 'action is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-pCComplianceposture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing details', (done) => {
        try {
          a.pCComplianceposture('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'details is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-pCComplianceposture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing policyIds', (done) => {
        try {
          a.pCComplianceposture('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'policyIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-pCComplianceposture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ips', (done) => {
        try {
          a.pCComplianceposture('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ips is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-pCComplianceposture', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assetGet - errors', () => {
      it('should have a assetGet function', (done) => {
        try {
          assert.equal(true, typeof a.assetGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assetSearchbycriteria - errors', () => {
      it('should have a assetSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.assetSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assetUpdate - errors', () => {
      it('should have a assetUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.assetUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostassetSearchbycriteria - errors', () => {
      it('should have a hostassetSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.hostassetSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostassetsGet - errors', () => {
      it('should have a hostassetsGet function', (done) => {
        try {
          assert.equal(true, typeof a.hostassetsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagGet - errors', () => {
      it('should have a tagGet function', (done) => {
        try {
          assert.equal(true, typeof a.tagGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagList - errors', () => {
      it('should have a tagList function', (done) => {
        try {
          assert.equal(true, typeof a.tagList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tagUpdate - errors', () => {
      it('should have a tagUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.tagUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAssetSearchbycriteria - errors', () => {
      it('should have a postAssetSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.postAssetSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postAssetSearchbycriteria(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAssetSearchbycriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchtofindtag - errors', () => {
      it('should have a searchtofindtag function', (done) => {
        try {
          assert.equal(true, typeof a.searchtofindtag === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fields', (done) => {
        try {
          a.searchtofindtag(null, null, (data, error) => {
            try {
              const displayE = 'fields is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-searchtofindtag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.searchtofindtag('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-searchtofindtag', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appCount - errors', () => {
      it('should have a appCount function', (done) => {
        try {
          assert.equal(true, typeof a.appCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appList - errors', () => {
      it('should have a appList function', (done) => {
        try {
          assert.equal(true, typeof a.appList === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appUpdate - errors', () => {
      it('should have a appUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.appUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appScan - errors', () => {
      it('should have a appScan function', (done) => {
        try {
          assert.equal(true, typeof a.appScan === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportCreatewebapp - errors', () => {
      it('should have a reportCreatewebapp function', (done) => {
        try {
          assert.equal(true, typeof a.reportCreatewebapp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reportDownload - errors', () => {
      it('should have a reportDownload function', (done) => {
        try {
          assert.equal(true, typeof a.reportDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanDownload - errors', () => {
      it('should have a scanDownload function', (done) => {
        try {
          assert.equal(true, typeof a.scanDownload === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scanListrunning - errors', () => {
      it('should have a scanListrunning function', (done) => {
        try {
          assert.equal(true, typeof a.scanListrunning === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPIstofetchFIMeventdataforignoredevents - errors', () => {
      it('should have a aPIstofetchFIMeventdataforignoredevents function', (done) => {
        try {
          assert.equal(true, typeof a.aPIstofetchFIMeventdataforignoredevents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.aPIstofetchFIMeventdataforignoredevents(null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.aPIstofetchFIMeventdataforignoredevents('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.aPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.aPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getignoredeventscount - errors', () => {
      it('should have a getignoredeventscount function', (done) => {
        try {
          assert.equal(true, typeof a.getignoredeventscount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getignoredeventscount(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.getignoredeventscount('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getignoredeventscount('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getignoredeventscount('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.getignoredeventscount('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchignoredeventdetails - errors', () => {
      it('should have a fetchignoredeventdetails function', (done) => {
        try {
          assert.equal(true, typeof a.fetchignoredeventdetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ignoredEventId', (done) => {
        try {
          a.fetchignoredeventdetails(null, (data, error) => {
            try {
              const displayE = 'ignoredEventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetchignoredeventdetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAPIstofetchFIMeventdataforignoredevents - errors', () => {
      it('should have a postAPIstofetchFIMeventdataforignoredevents function', (done) => {
        try {
          assert.equal(true, typeof a.postAPIstofetchFIMeventdataforignoredevents === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchAfter', (done) => {
        try {
          a.postAPIstofetchFIMeventdataforignoredevents('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'searchAfter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdataforignoredevents', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGetignoredeventscount - errors', () => {
      it('should have a postGetignoredeventscount function', (done) => {
        try {
          assert.equal(true, typeof a.postGetignoredeventscount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postGetignoredeventscount(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postGetignoredeventscount('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.postGetignoredeventscount('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.postGetignoredeventscount('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postGetignoredeventscount('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetignoredeventscount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchignoredeventdetails1 - errors', () => {
      it('should have a fetchignoredeventdetails1 function', (done) => {
        try {
          assert.equal(true, typeof a.fetchignoredeventdetails1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ignoredEventId', (done) => {
        try {
          a.fetchignoredeventdetails1(null, (data, error) => {
            try {
              const displayE = 'ignoredEventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetchignoredeventdetails1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPIstofetchFIMeventdata - errors', () => {
      it('should have a aPIstofetchFIMeventdata function', (done) => {
        try {
          assert.equal(true, typeof a.aPIstofetchFIMeventdata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.aPIstofetchFIMeventdata(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.aPIstofetchFIMeventdata('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.aPIstofetchFIMeventdata('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.aPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentContext', (done) => {
        try {
          a.aPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'incidentContext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentIds', (done) => {
        try {
          a.aPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'incidentIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#geteventcountwithinadaterange - errors', () => {
      it('should have a geteventcountwithinadaterange function', (done) => {
        try {
          assert.equal(true, typeof a.geteventcountwithinadaterange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.geteventcountwithinadaterange(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentContext', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'incidentContext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentIds', (done) => {
        try {
          a.geteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'incidentIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetcheventdetails - errors', () => {
      it('should have a fetcheventdetails function', (done) => {
        try {
          assert.equal(true, typeof a.fetcheventdetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventId', (done) => {
        try {
          a.fetcheventdetails(null, (data, error) => {
            try {
              const displayE = 'eventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventdetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetcheventsforanincident - errors', () => {
      it('should have a fetcheventsforanincident function', (done) => {
        try {
          assert.equal(true, typeof a.fetcheventsforanincident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.fetcheventsforanincident(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.fetcheventsforanincident('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.fetcheventsforanincident('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.fetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.fetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentId', (done) => {
        try {
          a.fetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'incidentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#geteventcountforanincident - errors', () => {
      it('should have a geteventcountforanincident function', (done) => {
        try {
          assert.equal(true, typeof a.geteventcountforanincident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.geteventcountforanincident(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.geteventcountforanincident('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.geteventcountforanincident('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.geteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.geteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentId', (done) => {
        try {
          a.geteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'incidentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-geteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFetcheventsforanincident - errors', () => {
      it('should have a postFetcheventsforanincident function', (done) => {
        try {
          assert.equal(true, typeof a.postFetcheventsforanincident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentId', (done) => {
        try {
          a.postFetcheventsforanincident(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'incidentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postFetcheventsforanincident('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.postFetcheventsforanincident('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.postFetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.postFetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postFetcheventsforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postFetcheventsforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGeteventcountforanincident - errors', () => {
      it('should have a postGeteventcountforanincident function', (done) => {
        try {
          assert.equal(true, typeof a.postGeteventcountforanincident === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentId', (done) => {
        try {
          a.postGeteventcountforanincident(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'incidentId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postGeteventcountforanincident('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postGeteventcountforanincident('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.postGeteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.postGeteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postGeteventcountforanincident('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountforanincident', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAPIstofetchFIMeventdata - errors', () => {
      it('should have a postAPIstofetchFIMeventdata function', (done) => {
        try {
          assert.equal(true, typeof a.postAPIstofetchFIMeventdata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postAPIstofetchFIMeventdata(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentContext', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'incidentContext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentIds', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'incidentIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchAfter', (done) => {
        try {
          a.postAPIstofetchFIMeventdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'searchAfter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMeventdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGeteventcountwithinadaterange - errors', () => {
      it('should have a postGeteventcountwithinadaterange function', (done) => {
        try {
          assert.equal(true, typeof a.postGeteventcountwithinadaterange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postGeteventcountwithinadaterange(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postGeteventcountwithinadaterange('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.postGeteventcountwithinadaterange('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postGeteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentContext', (done) => {
        try {
          a.postGeteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'incidentContext is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing incidentIds', (done) => {
        try {
          a.postGeteventcountwithinadaterange('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'incidentIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGeteventcountwithinadaterange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetcheventdetails1 - errors', () => {
      it('should have a fetcheventdetails1 function', (done) => {
        try {
          assert.equal(true, typeof a.fetcheventdetails1 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing eventId', (done) => {
        try {
          a.fetcheventdetails1(null, (data, error) => {
            try {
              const displayE = 'eventId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetcheventdetails1', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#aPIstofetchFIMincidentdata - errors', () => {
      it('should have a aPIstofetchFIMincidentdata function', (done) => {
        try {
          assert.equal(true, typeof a.aPIstofetchFIMincidentdata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.aPIstofetchFIMincidentdata(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.aPIstofetchFIMincidentdata('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.aPIstofetchFIMincidentdata('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.aPIstofetchFIMincidentdata('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.aPIstofetchFIMincidentdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-aPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getincidentcount - errors', () => {
      it('should have a getincidentcount function', (done) => {
        try {
          assert.equal(true, typeof a.getincidentcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getincidentcount(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.getincidentcount('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getincidentcount('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getincidentcount('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.getincidentcount('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAPIstofetchFIMincidentdata - errors', () => {
      it('should have a postAPIstofetchFIMincidentdata function', (done) => {
        try {
          assert.equal(true, typeof a.postAPIstofetchFIMincidentdata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postAPIstofetchFIMincidentdata(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.postAPIstofetchFIMincidentdata('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.postAPIstofetchFIMincidentdata('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing attributes', (done) => {
        try {
          a.postAPIstofetchFIMincidentdata('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'attributes is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postAPIstofetchFIMincidentdata('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postAPIstofetchFIMincidentdata', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGetincidentcount - errors', () => {
      it('should have a postGetincidentcount function', (done) => {
        try {
          assert.equal(true, typeof a.postGetincidentcount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.postGetincidentcount(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupBy', (done) => {
        try {
          a.postGetincidentcount('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'groupBy is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.postGetincidentcount('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing interval', (done) => {
        try {
          a.postGetincidentcount('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'interval is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.postGetincidentcount('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-postGetincidentcount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alertsSearchbycriteria - errors', () => {
      it('should have a alertsSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.alertsSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alertsViewdetailsbyid - errors', () => {
      it('should have a alertsViewdetailsbyid function', (done) => {
        try {
          assert.equal(true, typeof a.alertsViewdetailsbyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#detectionsCount - errors', () => {
      it('should have a detectionsCount function', (done) => {
        try {
          assert.equal(true, typeof a.detectionsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAlertsViewdetailsbyid - errors', () => {
      it('should have a getAlertsViewdetailsbyid function', (done) => {
        try {
          assert.equal(true, typeof a.getAlertsViewdetailsbyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alertsDownloadbycriteria - errors', () => {
      it('should have a alertsDownloadbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.alertsDownloadbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing format', (done) => {
        try {
          a.alertsDownloadbycriteria(null, (data, error) => {
            try {
              const displayE = 'format is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-alertsDownloadbycriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#profilesSearchbycriteria - errors', () => {
      it('should have a profilesSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.profilesSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#profilesViewdetailsbyid - errors', () => {
      it('should have a profilesViewdetailsbyid function', (done) => {
        try {
          assert.equal(true, typeof a.profilesViewdetailsbyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rulesetsSearchbycriteria - errors', () => {
      it('should have a rulesetsSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.rulesetsSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rulesetsViewdetailsbyid - errors', () => {
      it('should have a rulesetsViewdetailsbyid function', (done) => {
        try {
          assert.equal(true, typeof a.rulesetsViewdetailsbyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rulesSearchbycriteria - errors', () => {
      it('should have a rulesSearchbycriteria function', (done) => {
        try {
          assert.equal(true, typeof a.rulesSearchbycriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rulesViewdetailsbyid - errors', () => {
      it('should have a rulesViewdetailsbyid function', (done) => {
        try {
          assert.equal(true, typeof a.rulesViewdetailsbyid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCertviewCertificatesforagivencustomer - errors', () => {
      it('should have a getCertviewCertificatesforagivencustomer function', (done) => {
        try {
          assert.equal(true, typeof a.getCertviewCertificatesforagivencustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getCertviewCertificatesforagivencustomer(null, null, null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCertviewCertificatesforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageNumber', (done) => {
        try {
          a.getCertviewCertificatesforagivencustomer('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'pageNumber is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCertviewCertificatesforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.getCertviewCertificatesforagivencustomer('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCertviewCertificatesforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sort', (done) => {
        try {
          a.getCertviewCertificatesforagivencustomer('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'sort is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getCertviewCertificatesforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#retrievehostassessmentsummary - errors', () => {
      it('should have a retrievehostassessmentsummary function', (done) => {
        try {
          assert.equal(true, typeof a.retrievehostassessmentsummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing host', (done) => {
        try {
          a.retrievehostassessmentsummary(null, (data, error) => {
            try {
              const displayE = 'host is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-retrievehostassessmentsummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchdetailsofallassetsforagivencustomer - errors', () => {
      it('should have a fetchdetailsofallassetsforagivencustomer function', (done) => {
        try {
          assert.equal(true, typeof a.fetchdetailsofallassetsforagivencustomer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lastModifiedDate', (done) => {
        try {
          a.fetchdetailsofallassetsforagivencustomer(null, null, (data, error) => {
            try {
              const displayE = 'lastModifiedDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetchdetailsofallassetsforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing pageSize', (done) => {
        try {
          a.fetchdetailsofallassetsforagivencustomer('fakeparam', null, (data, error) => {
            try {
              const displayE = 'pageSize is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetchdetailsofallassetsforagivencustomer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fetchassetsdetailsforagivenassetid - errors', () => {
      it('should have a fetchassetsdetailsforagivenassetid function', (done) => {
        try {
          assert.equal(true, typeof a.fetchassetsdetailsforagivenassetid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing assetId', (done) => {
        try {
          a.fetchassetsdetailsforagivenassetid(null, (data, error) => {
            try {
              const displayE = 'assetId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-fetchassetsdetailsforagivenassetid', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getcountofassetssatisfyingthefiltercriteria - errors', () => {
      it('should have a getcountofassetssatisfyingthefiltercriteria function', (done) => {
        try {
          assert.equal(true, typeof a.getcountofassetssatisfyingthefiltercriteria === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.getcountofassetssatisfyingthefiltercriteria(null, null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getcountofassetssatisfyingthefiltercriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing lastModifiedDate', (done) => {
        try {
          a.getcountofassetssatisfyingthefiltercriteria('fakeparam', null, (data, error) => {
            try {
              const displayE = 'lastModifiedDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-qualys-adapter-getcountofassetssatisfyingthefiltercriteria', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
