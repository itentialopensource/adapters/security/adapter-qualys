
## 0.3.4 [10-15-2024]

* Changes made at 2024.10.14_19:52PM

See merge request itentialopensource/adapters/adapter-qualys!10

---

## 0.3.3 [09-15-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-qualys!8

---

## 0.3.2 [08-14-2024]

* Changes made at 2024.08.14_18:00PM

See merge request itentialopensource/adapters/adapter-qualys!7

---

## 0.3.1 [08-07-2024]

* Changes made at 2024.08.06_19:14PM

See merge request itentialopensource/adapters/adapter-qualys!6

---

## 0.3.0 [05-14-2024]

* 2024 Adapter Migration

See merge request itentialopensource/adapters/security/adapter-qualys!5

---

## 0.2.3 [03-27-2024]

* Changes made at 2024.03.27_13:38PM

See merge request itentialopensource/adapters/security/adapter-qualys!4

---

## 0.2.2 [03-11-2024]

* Changes made at 2024.03.11_16:12PM

See merge request itentialopensource/adapters/security/adapter-qualys!3

---

## 0.2.1 [02-27-2024]

* Changes made at 2024.02.27_11:45AM

See merge request itentialopensource/adapters/security/adapter-qualys!2

---

## 0.2.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-qualys!1

---

## 0.1.1 [09-27-2022]

* Bug fixes and performance improvements

See commit 4113da8

---
