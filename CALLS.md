## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Qualys. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Qualys.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Qualys. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">testVMAPIAccess(callback)</td>
    <td style="padding:15px">Test VM API Access</td>
    <td style="padding:15px">{base_path}/{version}/msp/user_list.php?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">testKnowledgeBaseAccess(action, details, ids, echoRequest, callback)</td>
    <td style="padding:15px">Test KnowledgeBase Access</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/knowledge_base/vuln?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getyourportalversion(callback)</td>
    <td style="padding:15px">Get your portal version</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/portal/version?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1DownloadaListofallHoststobePurgedForyourrecords(action, echoRequest, noVmScanSince, callback)</td>
    <td style="padding:15px">Step 1: Download a List of all Hosts to be Purged (for your records)</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/host?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">step1LaunchReportthatSupportRequested(action, templateId, reportTitle, outputFormat, callback)</td>
    <td style="padding:15px">Step 1: Launch Report that Support Requested</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addUsersNotbulk(action, sendEmail, userRole, firstName, lastName, title, phone, email, address1, city, country, state, callback)</td>
    <td style="padding:15px">Add Users (not bulk)</td>
    <td style="padding:15px">{base_path}/{version}/msp/user.php?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bulkActivateAgentsforVMModule(module, callback)</td>
    <td style="padding:15px">Bulk Activate Agents for VM Module</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/activate/am/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">exportNewActiveReOpenVulnerabilityDatausingHLD(action, ids, status, detectionUpdatedSince, truncationLimit, outputFormat, callback)</td>
    <td style="padding:15px">Export New, Active, Re-Open Vulnerability Data using HLD</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/host/vm/detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadallcloudagentactivationkeys(callback)</td>
    <td style="padding:15px">Download all cloud agent activation keys</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/ca/agentactkey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadtheQualysuseractivitylog(action, callback)</td>
    <td style="padding:15px">Download the Qualys user activity log</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/activity_log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listAlertsbycriteriaFirstbatchexample(callback)</td>
    <td style="padding:15px">List Alerts by criteria (first batch example)</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/cm/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">enableVulnerabilityManagementandPolicyComplianceHostAssets(action, enableVm, enablePc, ips, callback)</td>
    <td style="padding:15px">Enable Vulnerability Management and Policy Compliance Host Assets</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/ip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPICommandstoaddIPaddressEsWindows(action, ids, addIps, callback)</td>
    <td style="padding:15px">API Commands to add IP address(es) (Windows)</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/auth/windows?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPICommandstoaddIPaddressEsUnixLinux(action, ids, addIps, callback)</td>
    <td style="padding:15px">API Commands to add IP address(es) (Unix/Linux)</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/auth/unix?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchVulnerabilityScan(action, iscannerId, scanTitle, optionId, ip, priority, callback)</td>
    <td style="padding:15px">Launch Vulnerability Scan</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/scan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">launchPolicyComplianceScan(action, iscannerId, scanTitle, optionId, ip, priority, callback)</td>
    <td style="padding:15px">Launch Policy Compliance Scan</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/scan/compliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">optionalListallExistingAssetGroups(action, outputFormat, truncationLimit, callback)</td>
    <td style="padding:15px">(Optional) List all Existing Asset Groups</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createanewDynamicTag(body, callback)</td>
    <td style="padding:15px">Create a new Dynamic Tag</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/create/am/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchHostsbyTagName(body, callback)</td>
    <td style="padding:15px">Search Hosts by Tag Name</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/search/am/hostasset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyingatagortagstomultiplehostassetsHostID(body, callback)</td>
    <td style="padding:15px">Applying a tag or tags to multiple host assets (Host ID)</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/update/am/hostasset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">generateAuthenticateGenerateToken(username, password, token, callback)</td>
    <td style="padding:15px">Generate Authenticate Generate Token</td>
    <td style="padding:15px">{base_path}/{version}/auth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduleCertViewScanbyFQDNs(action, scanTitle, active, optionId, scanType, fqdn, occurrence, frequencyWeeks, weekdays, startHour, startMinute, observeDst, callback)</td>
    <td style="padding:15px">Schedule CertView Scan by FQDNs</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/schedule/scan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">downloadallScheduledReportInfo(action, callback)</td>
    <td style="padding:15px">Download all Scheduled Report Info</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/schedule/scan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievedetailedhostinstanceinformation(fqdn, ip, port, service, callback)</td>
    <td style="padding:15px">Retrieve detailed host instance information</td>
    <td style="padding:15px">{base_path}/{version}/getEndpointData?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentIOCEventswhereIndicatorScoreisGreaterthan7(state, filter, callback)</td>
    <td style="padding:15px">Get Current IOC Events where Indicator Score is Greater than 7</td>
    <td style="padding:15px">{base_path}/{version}/ioc/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCurrentIOCEventsCountwhereIndicatorScoreisGreaterthan7(state, filter, callback)</td>
    <td style="padding:15px">Get Current IOC Events Count where Indicator Score is Greater than 7</td>
    <td style="padding:15px">{base_path}/{version}/ioc/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">excludedhostslist(action, callback)</td>
    <td style="padding:15px">Excluded hosts list</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/excluded_ip?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vMScannerappliances(action, callback)</td>
    <td style="padding:15px">VM, scanner appliances</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">excludedvirtualhostslist(action, callback)</td>
    <td style="padding:15px">Excluded virtual hosts list</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/asset/excluded_ip/history?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">pCComplianceposture(action, details, policyIds, ips, callback)</td>
    <td style="padding:15px">PC, compliance posture</td>
    <td style="padding:15px">{base_path}/{version}/api/2.0/fo/compliance/posture/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assetGet(callback)</td>
    <td style="padding:15px">Asset, get</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/am/asset/21325?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assetSearchbycriteria(callback)</td>
    <td style="padding:15px">Asset, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/am/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">assetUpdate(callback)</td>
    <td style="padding:15px">Asset, update</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/update/am/asset/32254111?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostassetSearchbycriteria(callback)</td>
    <td style="padding:15px">Host asset, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/am/hostasset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostassetsGet(callback)</td>
    <td style="padding:15px">Host assets, get</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/am/hostasset/24115?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagGet(callback)</td>
    <td style="padding:15px">Tag, get</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/am/tag/7813875?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagList(callback)</td>
    <td style="padding:15px">Tag, list</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/am/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tagUpdate(callback)</td>
    <td style="padding:15px">Tag, update</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/update/am/tag/7813875?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetSearchbycriteria(body, callback)</td>
    <td style="padding:15px">Asset, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/search/am/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchtofindtag(fields, body, callback)</td>
    <td style="padding:15px">Search to find tag</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/2.0/search/am/tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appCount(callback)</td>
    <td style="padding:15px">App, count</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/get/was/webapp/24115?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appList(callback)</td>
    <td style="padding:15px">App, list</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/search/was/webapp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appUpdate(callback)</td>
    <td style="padding:15px">App, update</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/update/was/webapp/21325?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appScan(callback)</td>
    <td style="padding:15px">App, scan</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/launch/was/wasscan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportCreatewebapp(callback)</td>
    <td style="padding:15px">Report, create webapp</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/create/was/report?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reportDownload(callback)</td>
    <td style="padding:15px">Report, download</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/download/was/report/123057?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanDownload(callback)</td>
    <td style="padding:15px">Scan, download</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/download/was/wasscan/4912497?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scanListrunning(callback)</td>
    <td style="padding:15px">Scan, list running</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/3.0/search/was/wasscan?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPIstofetchFIMeventdataforignoredevents(filter, pageNumber, pageSize, sort, callback)</td>
    <td style="padding:15px">APIs to fetch FIM event data for ignored events</td>
    <td style="padding:15px">{base_path}/{version}/events/ignore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getignoredeventscount(filter, groupBy, limit, sort, interval, callback)</td>
    <td style="padding:15px">Get ignored events count</td>
    <td style="padding:15px">{base_path}/{version}/events/ignore/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchignoredeventdetails(ignoredEventId, callback)</td>
    <td style="padding:15px">Fetch ignored event details</td>
    <td style="padding:15px">{base_path}/{version}/events/ignore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAPIstofetchFIMeventdataforignoredevents(filter, groupBy, pageNumber, pageSize, sort, searchAfter, callback)</td>
    <td style="padding:15px">APIs to fetch FIM event data for ignored events</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/ignore/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetignoredeventscount(filter, groupBy, limit, interval, sort, callback)</td>
    <td style="padding:15px">Get ignored events count</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/ignore/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchignoredeventdetails1(ignoredEventId, callback)</td>
    <td style="padding:15px">Fetch ignored event details1</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/ignore/{ignoredEventId&#39;}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPIstofetchFIMeventdata(filter, pageNumber, pageSize, sort, incidentContext, incidentIds, callback)</td>
    <td style="padding:15px">APIs to fetch FIM event data</td>
    <td style="padding:15px">{base_path}/{version}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">geteventcountwithinadaterange(filter, groupBy, limit, sort, interval, incidentContext, incidentIds, callback)</td>
    <td style="padding:15px">Get event count within a date range</td>
    <td style="padding:15px">{base_path}/{version}/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetcheventdetails(eventId, callback)</td>
    <td style="padding:15px">Fetch event details</td>
    <td style="padding:15px">{base_path}/{version}/events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetcheventsforanincident(filter, pageNumber, pageSize, sort, attributes, incidentId, callback)</td>
    <td style="padding:15px">Fetch events for an incident</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/events?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">geteventcountforanincident(filter, groupBy, limit, sort, interval, incidentId, callback)</td>
    <td style="padding:15px">Get event count for an incident.</td>
    <td style="padding:15px">{base_path}/{version}/incidents/{pathv1}/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postFetcheventsforanincident(incidentId, filter, pageNumber, pageSize, attributes, sort, callback)</td>
    <td style="padding:15px">Fetch events for an incident</td>
    <td style="padding:15px">{base_path}/{version}/v2/incidents/{pathv1}/events/search&#39;?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeteventcountforanincident(incidentId, filter, groupBy, limit, interval, sort, callback)</td>
    <td style="padding:15px">Get event count for an incident.</td>
    <td style="padding:15px">{base_path}/{version}/v2/incidents/{pathv1}/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAPIstofetchFIMeventdata(filter, groupBy, pageNumber, pageSize, sort, incidentContext, incidentIds, searchAfter, callback)</td>
    <td style="padding:15px">APIs to fetch FIM event data</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGeteventcountwithinadaterange(filter, groupBy, interval, sort, incidentContext, incidentIds, callback)</td>
    <td style="padding:15px">Get event count within a date range</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetcheventdetails1(eventId, callback)</td>
    <td style="padding:15px">Fetch event details1</td>
    <td style="padding:15px">{base_path}/{version}/v2/events/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">aPIstofetchFIMincidentdata(filter, pageNumber, pageSize, sort, attributes, callback)</td>
    <td style="padding:15px">APIs to fetch FIM incident data.</td>
    <td style="padding:15px">{base_path}/{version}/incidents?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getincidentcount(filter, groupBy, limit, sort, interval, callback)</td>
    <td style="padding:15px">Get incident count</td>
    <td style="padding:15px">{base_path}/{version}/incidents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAPIstofetchFIMincidentdata(filter, pageNumber, pageSize, attributes, sort, callback)</td>
    <td style="padding:15px">APIs to fetch FIM incident data.</td>
    <td style="padding:15px">{base_path}/{version}/v2/incidents/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGetincidentcount(filter, groupBy, limit, interval, sort, callback)</td>
    <td style="padding:15px">Get incident count</td>
    <td style="padding:15px">{base_path}/{version}/v2/incidents/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertsSearchbycriteria(callback)</td>
    <td style="padding:15px">Alerts, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/md/detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertsViewdetailsbyid(callback)</td>
    <td style="padding:15px">Alerts, view details by id</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/md/detection/37747097?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">detectionsCount(callback)</td>
    <td style="padding:15px">Detections, count</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/count/md/detection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertsViewdetailsbyid(callback)</td>
    <td style="padding:15px">Alerts, view details by id</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/cm/alert/246213?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alertsDownloadbycriteria(format, callback)</td>
    <td style="padding:15px">Alerts, download by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/download/cm/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">profilesSearchbycriteria(callback)</td>
    <td style="padding:15px">Profiles, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/cm/profile?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">profilesViewdetailsbyid(callback)</td>
    <td style="padding:15px">Profiles, view details by id</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/cm/profile/7401?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rulesetsSearchbycriteria(callback)</td>
    <td style="padding:15px">Rulesets, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/cm/ruleset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rulesetsViewdetailsbyid(callback)</td>
    <td style="padding:15px">Rulesets, view details by id</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/cm/ruleset/4001?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rulesSearchbycriteria(callback)</td>
    <td style="padding:15px">Rules, search by criteria</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/search/cm/rule?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rulesViewdetailsbyid(callback)</td>
    <td style="padding:15px">Rules, view details by id</td>
    <td style="padding:15px">{base_path}/{version}/qps/rest/1.0/get/cm/rule/6002?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCertviewCertificatesforagivencustomer(filter, pageNumber, pageSize, sort, callback)</td>
    <td style="padding:15px">Get Certview Certificates for a given customer</td>
    <td style="padding:15px">{base_path}/{version}/certificates?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">retrievehostassessmentsummary(host, callback)</td>
    <td style="padding:15px">Retrieve host assessment summary</td>
    <td style="padding:15px">{base_path}/{version}/analyze?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchdetailsofallassetsforagivencustomer(lastModifiedDate, pageSize, callback)</td>
    <td style="padding:15px">Fetch details of all assets for a given customer</td>
    <td style="padding:15px">{base_path}/{version}/am/v1/assets/host/list?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fetchassetsdetailsforagivenassetid(assetId, callback)</td>
    <td style="padding:15px">Fetch assets details for a given asset id</td>
    <td style="padding:15px">{base_path}/{version}/am/v1/asset/host/id?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getcountofassetssatisfyingthefiltercriteria(filter, lastModifiedDate, callback)</td>
    <td style="padding:15px">Get count of assets satisfying the filter criteria</td>
    <td style="padding:15px">{base_path}/{version}/am/v1/assets/host/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
